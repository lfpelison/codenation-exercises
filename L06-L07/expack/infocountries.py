import requests


def requestdata():
    r = requests.get('https://restcountries.eu/rest/v2/all')
    data = r.json()
    return data


def countrieslist():
    data = requestdata()
    countries_list = []
    for i in data:
        countries_list.append({'code': i.get('alpha3Code'), 'name': i.get('translations').get('br'), 'region': i.get('region'), 'population': i.get('population'), 'area': i.get('area')})
    return countries_list


def countrieslist_byregion(region):
    data = requestdata()
    countries_list = []
    for i in data:
        if i.get('region') == region:
            countries_list.append({'code': i.get('alpha3Code'), 'name': i.get('translations').get('br'), 'region': i.get('region'), 'population': i.get('population'), 'area': i.get('area')})
    return countries_list


def countrieslist_bypop(min):
    data = requestdata()
    countries_list = []
    for i in data:
        if i.get('population') >= min:
            countries_list.append({'code': i.get('alpha3Code'), 'name': i.get('translations').get('br'), 'region': i.get('region'), 'population': i.get('population'), 'area': i.get('area')})
    return countries_list


def regions_resume(region):
    data = requestdata()
    regions = countrieslist_byregion(region)
    number_countries = len(regions)
    number_population = 0
    number_area = 0
    for i in regions:
        number_population += i.get('population')
        if i.get('area'):
            number_area += i.get('area')
    return {'number of countries': number_countries, 'total population': number_population, 'total area': number_area}
