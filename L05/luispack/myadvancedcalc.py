import math


def avg(x1, x2):
    return (x1 + x2) / 2.0


def sqrt(x1):
    return math.sqrt(x1)


def factorial(x1):
    return math.factorial(x1)


def floor(x1):
    return math.floor(x1)
