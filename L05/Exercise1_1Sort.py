from sys import argv
from luispack import openfile_as_list

script, filename = argv

# Give the name of the file in the bash, like
# python Exercise1_Luis.py words3.txt

list_archive = openfile_as_list.do_it(filename)

# Set the list to remove duplicated itens
set_archive = set(list_archive)

# Return the set to a list to sort later
list_archive = list(set_archive)

# sort words into the file
list_archive.sort()


lenght_list = len(list_archive)


# print one word in one line
print "".join(list_archive)
print "This archive, named by " + filename + " has " + str(lenght_list) + " lines."
