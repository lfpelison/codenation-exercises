# Exercises LESSON 05

## Exercício 1 -> L05

Instruções:

* Não usar ifs ou estruturas de laço(for, while)
* O nome do arquivo deve ser enviado como argumento

Criar scripts python para as seguintes ferramentas:

1. Ordenar conteúdo do arquivo recebendo como input nome do arquivo (sort)

1. Remover itens repetidos do arquivo (uniq)

1. Conte o número de linhas do arquivo (wc -l)

---

## Exercício 2 -> L05/luispack

1. Criar módulo calculadora com pelo menos 4 operações simples e um submódulo de calculadora avançada com ao menos duas operações.

---

## Exercício 3 -> L05

1. Crie um programa que aceite uma sequencia de números separados por vírgula como argumento e gere uma lista e uma tupla com todos os elementos.
Imprima o resultado.

1. Escreva um programa que aceite um texto como entrada(stdin) e transforme o texto em todo maiúsculo.

---

# Exercises LESSON 06

## Exercício -> L06-L07

1. Construir um módulo para acessar informações sobre os países do mundo disponível em `https://restcountries.eu/rest/v2/all`

* Deve permitir listar os países do mundo
* Deve permitir listar os países filtrando por continente
* Deve permitir listar os países filtrando por população mínima

    O resultado para todas as consultas acima deverá ser uma lista de dicionários contendo:
        * Código de 3 dígitos do país
        * Nome
        * Continente
        * População
        * Area

* Deve permitir fazer resumo sobre um continente escolhido(argumento) com os seguintes dados:
    * Número de países
    * População total
    * Area total


---
